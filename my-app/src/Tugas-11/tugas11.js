import React, {Component} from 'react'

class Timer extends Component{
  constructor(props){
    super(props)
    let time = new Date()
    time = time.toLocaleTimeString('id-ID', { hour12:true })
    this.state = {
        timer: 10,
        timeNow: time,
        showTime: true
    }
  }

  componentDidMount(){
    if (this.props.start !== undefined){
      this.setState({time: this.props.start})
    }
    this.timerID = setInterval(
      () => this.tick(),
      1000
    )
    this.timeNow = setInterval(
    () => this.now(), 
    1000)
  }

  componentDidUpdate() {
    if (this.state.timer === 0 && this.state.showTime) {
        this.hideTime();
        this.stopTime();
    }
}

  componentWillUnmount(){
    clearInterval(this.timerID);
  }

  tick() {
    this.setState({
      timer: this.state.timer - 1 
    });
  }

  now() {
    let time = new Date()
    time = time.toLocaleString('ID', { hour: 'numeric', minute: 'numeric', second:'numeric', hour12: true }).replace(/\./g,":")
    this.setState({ timeNow: time })
}

stopTime() {
    this.componentWillUnmount()
}

hideTime() {
    this.setState({ showTime: false })
}

  render(){
    return(
      <>
        <h1 style={{textAlign: "center"}}>
            <p style={{float:"left", margin:"120px"}}>Sekarang Jam {this.state.timeNow}</p>
            <p style={{float:"left", margin:"120px"}}>Hitung mundur {this.state.timer}</p>
        </h1>
      </>
    )
  }
}

export default Timer