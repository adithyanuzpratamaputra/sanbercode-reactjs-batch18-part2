import React, {useState} from 'react';
import './App.css';
//import Tugas9 from './Tugas-9/tugas9'
//import TabelHarga from './Tugas-10/TabelHarga'
//import Timer from './Tugas-11/tugas11'
//import Lists from './Tugas-12/tugas12'
//import List from './Tugas-13/tugas13'
import Buah from './Tugas-14/Buah';
import { BrowserRouter as Router } from "react-router-dom";
import Nav from './Tugas-15/Nav';
import Routes from './Tugas-15/Routes';
import Tugas9 from './Tugas-9/tugas9';


function App() {
  return (
    <div className="Apps">
      <Router>
        <Nav/>
        <Routes/>
      </Router>

      {/*<Lists />*/}
      {/*<List />*/}
      <Buah />

      {/*<Tugas9 />
      <TabelHarga />
      <Timer /> */}
    </div>
  );
}

export default App;
