import React from 'react';
import { Switch, Route } from "react-router"
import Tugas9 from '../Tugas-9/tugas9'
import Timer from '../Tugas-11/tugas11'
import List from '../Tugas-13/tugas13'
import Buah from '../Tugas-14/Buah'
import tabelHarga from '../Tugas-10/TabelHarga'
import Lists from '../Tugas-12/tugas12';

const Routes = () => {
    return (
        <div>
            <Switch>
                <Route exact path="/">
                    <Tugas9/>
                </Route>
                <Route path="/tugas-9">
                    <Tugas9/>
                </Route>
                <Route path="/tugas-10">
                    <tabelHarga/>
                </Route>
                <Route path="/tugas-11">
                    <Timer/>
                </Route>
                <Route path="/tugas-12">
                    <Lists/>
                </Route>
                <Route path="/tugas-13">
                    <List/>
                </Route>
                <Route path="/tugas-14">
                    <Buah/>
                </Route>
            </Switch>
        </div>
    )
}

export default Routes